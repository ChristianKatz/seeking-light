﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Particle : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem[] particles;

    [SerializeField]
    private GameObject blueImage;

    [SerializeField]
    private Vector3 playerPosition;

    private float DistanceToPlayer;

    private bool mageClose;

    bool area01Empty;

    bool area02Empty;

    bool area03Empty;

    bool area04Empty;

    bool area05Empty;

    AudioSource[] citySounds;

    [SerializeField]
    int soundList;

    [SerializeField]
    private Vector3 particlePosition;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        float DistanceToPlayer = Vector3.Distance(playerPosition, particlePosition);

        distantParticles();
    }

    private void distantParticles()
    {
        if(DistanceToPlayer < 80 && mageClose == true)
        {
            blueImage.gameObject.SetActive(true);           
        }
        else if (DistanceToPlayer > 100 && mageClose == true)
        {
            for (int i = 0; i < soundList; i++)
            {
                citySounds[i].volume = Mathf.Lerp(1f, 0.1f, Time.time);
            }

        }
        else if (DistanceToPlayer > 150 && mageClose == false)
        {
            mageClose = true;
        }
        else if (mageClose == true)
        {
            mageClose = false;           
        }
        else if (DistanceToPlayer > 300 && mageClose == false)
        {
            if(area01Empty == false)
            {
                particles[1].Play();    
            }

            if (area02Empty == false)
            {
                particles[2].Play();
            }

            if (area03Empty == false)
            {
                particles[3].Play();
            }

            if (area04Empty == false)
            {
                particles[4].Play();
            }

            if (area05Empty == false)
            {
                particles[5].Play();
            }

        }
        else
        {
            return;
        }
    }
}
