﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWalls : MonoBehaviour
{

    [SerializeField]
    private GameObject pressFButtonText;

    [SerializeField]
    private GameObject whatWillHappenText;

    [SerializeField]
    private GameObject people;


    // Start is called before the first frame update
    void Start()
    {
        pressFButtonText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerMove"))
        {
            pressFButtonText.SetActive(true);
            whatWillHappenText.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            pressFButtonText.SetActive(false);
            whatWillHappenText.SetActive(false);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            if(Input.GetKeyDown(KeyCode.F))                   
            {
                pressFButtonText.SetActive(false);
                whatWillHappenText.SetActive(false);
                people.SetActive(true);
                Destroy(gameObject);
            }
                
        }
    }

}
