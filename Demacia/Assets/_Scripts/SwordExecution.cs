﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SwordExecution : MonoBehaviour
{
    [SerializeField]
    private GameObject sword;

    [SerializeField]
    private GameObject pressFText;

    public GameObject Sword
    {
        get
        {
            return sword;
        }
        set
        {
            sword = value;
        }
    }

    [SerializeField]
    private Transform fallDestination;

    [SerializeField]
    private Transform player;

    [SerializeField]
    private int rotateSpeed;

    float playerDistance;

    [SerializeField]
    private float fallSpeed;

    [SerializeField]
    private AudioSource demacia;

    Suspicious suspicious;

    private bool dontKill = true;

    private bool swordRotation;

    private bool swordFallsDown;

    private float swordDeactivationCooldwon = 3;

    [SerializeField]
    private TextMeshProUGUI askForArrestText;
    public TextMeshProUGUI AskForArrestText
    {
        get
        {
            return askForArrestText;
        }
        set
        {
            askForArrestText = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        sword.SetActive(false);
        suspicious = FindObjectOfType<Suspicious>();
        swordFallsDown = false;
        swordRotation = false;
        askForArrestText.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
        pressFText.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        Arrest();
        SwordRotate();
        SwordGoesDown();
        playerDistance = Vector3.Distance(transform.position, player.transform.position);

    }

    void Arrest()
    {
        if(swordFallsDown == false)
        {
            if (Input.GetKeyDown(KeyCode.F) && playerDistance < 20)
            {

                // able to arrest him
                if (dontKill == true)
                {
                    askForArrestText.gameObject.GetComponent<TextMeshProUGUI>().enabled = true;
                    dontKill = false;
                    swordRotation = true;
                    sword.SetActive(true);
                    pressFText.SetActive(false);
                }

                // don't arrest him
                else
                {
                    askForArrestText.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
                    dontKill = true;
                    swordRotation = true;
                    sword.SetActive(false);
                    pressFText.SetActive(true);
                }
            }
            else if (dontKill == false)
            {
                //arrested
                if (Input.GetMouseButtonDown(0))
                {
                    askForArrestText.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
                    //suspicious.ArrestedText.gameObject.SetActive(true);
                    suspicious.MageArrested = true;
                    swordFallsDown = true;
                    demacia.Play();
                    pressFText.SetActive(false);
                }
            }

        }
    }
    void SwordRotate()
    {
        if(swordRotation)
        {
            sword.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        }       
    }

    void SwordGoesDown()
    {
        if(swordFallsDown == true)
        {
            swordDeactivationCooldwon -= Time.deltaTime;

            Sword.transform.position = Vector3.MoveTowards(sword.transform.position, fallDestination.position, fallSpeed * Time.deltaTime);

            if(swordDeactivationCooldwon <= 0)
            {
                sword.SetActive(false);
            }
        }

    }

}
