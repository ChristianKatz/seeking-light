﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    [SerializeField]
    private GameObject menu;

    private bool menuActivation;

    [SerializeField]
    private string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        MenuActivation();
    }

    void MenuActivation()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(menuActivation == true)
            {
                menu.SetActive(true);
                menuActivation = false;
            }

            else
            {
                menu.SetActive(false);
                menuActivation = true;
            }
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(sceneName);
        Time.timeScale = 1;
    }


}
