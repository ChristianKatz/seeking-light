﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceChanger : MonoBehaviour
{
    [SerializeField]
    private Transform[] people;

    [SerializeField]
    private Transform[] places;

    private int randomNumber;

    // Start is called before the first frame update
    void Start()
    {
        randomNumber = Random.Range(1, 5);
        ChangePlaces();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ChangePlaces()
    {
        switch(randomNumber)
        {
            case 1:
                for (int i = 0; i < people.Length; i++)
                {
                    people[i].position = places[i].position;
                }
                break;

            case 2:
                people[0].position = places[2].position;
                people[1].position = places[1].position;
                people[2].position = places[3].position;
                people[3].position = places[0].position;
                break;

            case 3:
                people[0].position = places[1].position;
                people[1].position = places[3].position;
                people[2].position = places[0].position;
                people[3].position = places[2].position;
                break;

            case 4:
                people[0].position = places[3].position;
                people[1].position = places[0].position;
                people[2].position = places[1].position;
                people[3].position = places[2].position;
                break;

            case 5:
                people[0].position = places[2].position;
                people[1].position = places[1].position;
                people[2].position = places[0].position;
                people[3].position = places[3].position;
                break;

        }



    }
}
