﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the player controlls
public class PlayerMovement : MonoBehaviour
{
    // the run speed of the character
    [SerializeField]
    public int runSpeed;

    // the speed with that the player can rotate the character
    [SerializeField]
    private int playerRotationSpeed;

    // contains the input value of the mouse
    private float PlayerRotation;

    // the new texture for the cursor
    //[SerializeField]
  //  public Texture2D CursorTexture;

    // float value, how fast the camera shoud be moved
    [SerializeField]
    private float cameraRotationSpeed;

    // float value to clamp the camera angle
    private float yAxisClamp;

    // Transform to have the camera position
    [SerializeField]
    private Transform CameraTransform;

    void Start()
    {
          // the texture for the cursor
     //   Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }

    void Update()
    {
        MoveCharacter();
        CameraMovement();
        
       
    }

    void MoveCharacter()
    {
        // get the inputs for the movement of the character
        PlayerRotation = Input.GetAxis("Mouse X") * playerRotationSpeed * Time.deltaTime;
        float moveHorizontal = Input.GetAxis("Horizontal") * runSpeed * Time.deltaTime;
        float moveVertical = Input.GetAxis("Vertical") * runSpeed * Time.deltaTime;

        // the character gets the values to move
        transform.Translate(moveHorizontal, 0, moveVertical);
        transform.Rotate(0, PlayerRotation, 0);
    }

    void CameraMovement()
    {
        // get the float value, where the mouse is moved at a certain speed
        float moveVertical = Input.GetAxis("Mouse Y") * -cameraRotationSpeed * Time.deltaTime;

        // get the value in which position the mouse is in the moment
        yAxisClamp += moveVertical;

        // clamp the angle 
        yAxisClamp = Mathf.Clamp(yAxisClamp, -45f, 45f);

        // the camera can be moved between these certain angles
        Quaternion CameraRotation = Quaternion.Euler(yAxisClamp, 0f, 0f);

        // the angle will be transmitted to the camera
        CameraTransform.localRotation = CameraRotation;
    }
}
