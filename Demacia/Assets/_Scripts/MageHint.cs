﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageHint : MonoBehaviour
{
    [SerializeField]
    private GameObject letterText;

    
   // private AudioSource hintSound;

    private bool fButton = true;

    private bool playerOnTrigger = false;


   // private GameObject magicDetectorScript;

    [SerializeField]
    private GameObject pressFText;

    // Start is called before the first frame update
    void Start()
    {
        letterText.SetActive(false);
        //hintSound.Stop();
        pressFText.SetActive(false);
/*
        if (magicDetectorScript.gameObject != null)
        {
            magicDetectorScript.gameObject.GetComponent<MagicDetector>().enabled = false;
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        ReadHint();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            pressFText.SetActive(true);
            playerOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            playerOnTrigger = false;
            letterText.SetActive(false);
            fButton = true;
            pressFText.SetActive(false);
        }
    }

    private void ReadHint()
    {
        if(playerOnTrigger == true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (fButton == true)
                {
                    letterText.SetActive(true);
                    //hintSound.Play();

               /*     if(magicDetectorScript.gameObject != null)
                    {
                        magicDetectorScript.gameObject.GetComponent<MagicDetector>().enabled = true;
                    }

              */
                    fButton = false;
                    pressFText.SetActive(false);
                }
                else
                {
                    letterText.SetActive(false);
                    fButton = true;
                    pressFText.SetActive(true);
                }
            }

        }

    }
}
