﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MageCounter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI mageCounterText;

    [SerializeField]
    private TextMeshProUGUI winText;

    [SerializeField]
    private GameObject menu;

    [SerializeField]
    private AudioSource FinalVoiceLine;

    private int mageCounterNumber = 0;
    public int MageCounterNumber
    {
        get
        {
            return mageCounterNumber;
        }
        set
        {
            mageCounterNumber = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        winText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        mageCounterText.text = string.Format("Mage arrested: " + mageCounterNumber + " /5");
        WindCondition();
    }

    void WindCondition()
    {
        if(mageCounterNumber == 5)
        {
            winText.gameObject.SetActive(true);
            StartCoroutine(LastWords());
        }
    }

    IEnumerator LastWords()
    {
        yield return new WaitForSeconds(2f);
        FinalVoiceLine.Play();
        menu.SetActive(true);
        Time.timeScale = 0;

    }
}
