﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPeople : MonoBehaviour
{
    [SerializeField]
    private GameObject lost;

    [SerializeField]
    private GameObject menu;

    [SerializeField]
    private Transform player;

    private float distance;
    
    // Start is called before the first frame update
    void Start()
    {
        lost.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(transform.position, player.position);

        if (distance <= 1.15f)
        {

            lost.SetActive(true);
            menu.SetActive(true);
            Time.timeScale = 0;
            Destroy(gameObject);

        }
    }




    
}
